/* Azadeh Ahmadi 1811386*/

	package movies.importer;
	
	import java.util.ArrayList;
	
	
	
	
	public class RemoveDuplicates extends Processor{
		
		public RemoveDuplicates (String sourceDir, String outputDir) {	
			super( sourceDir, outputDir, false);}
		public  ArrayList<String> process(ArrayList<String> input){
			ArrayList<String> noDuplicate = new ArrayList<String>();
			for (int i=0; i<input.size();i++) {
				String x=input.get(i);
				if(!noDuplicate.contains(x))
				noDuplicate.add(x);}
	return noDuplicate;
	}
	}